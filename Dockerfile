FROM node:alpine AS build
WORKDIR /app
ARG be_server
ENV REACT_APP_API_URL=$be_server
COPY package.json .
RUN npm install --force
COPY . .
RUN npm run build

FROM nginxinc/nginx-unprivileged:alpine3.19-perl AS deploy
COPY --from=build /app/build /usr/share/nginx/html
COPY online-shop-frontend.conf /etc/nginx/conf.d/
EXPOSE 3000
CMD ["nginx", "-g", "daemon off;"]

