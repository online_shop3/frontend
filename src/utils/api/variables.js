const API_URL = process.env.REACT_APP_API_URL;

export const variables = {
    BASE_URL: `${API_URL}/api/`,
    USER_API: `${API_URL}/api/user`,
    PRODUCT_API: `${API_URL}/api/product`,
    ORDER_API: `${API_URL}/api/order`,
    PRODUCTSIZE_API: `${API_URL}/api/productsize`,
    ORDERITEM_API: `${API_URL}/api/orderitem`,
};

